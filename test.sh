#!/bin/bash

urls=("http://localhost" "http://localhost:3001" "http://localhost:3001/people")

for url in "${urls[@]}"; do
  status=$(curl --write-out '%{http_code}' --silent --output /dev/null $url)

  if [ $status -eq 200 ]
  then
    echo "OK: $url"
  else
    echo $status
  fi
done