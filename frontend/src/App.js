import axios from 'axios';
import { useEffect, useState } from 'react';

function App() {
  const [people, setPeople] = useState([]);
  const [name, setName] = useState('');

  useEffect(() => {
    async function fetchData() {
      const { data } = await axios.get(`api/people`);
      setPeople(data);
    }
    fetchData();
  }, [people.length]);

  async function handleAddPerson() {
    const { data } = await axios.post(`api/person`, {
      name,
    });

    setPeople(prev => [...prev, data]);
  }

  return (
    <div>
      <ul>
        {people.map(person => (
          <li key={person._id}>{person.name}</li>
        ))}
      </ul>
      <input type="text" value={name} onChange={e => setName(e.target.value)} />
      <button onClick={handleAddPerson}>Add</button>
    </div>
  );
}

export default App;
