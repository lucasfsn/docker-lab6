import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';

const app = express();

app.use(express.json());
app.use(cors());

const Person = mongoose.model('Person', new mongoose.Schema({ name: String }));

app.post('/person', async (req, res) => {
  const { name } = req.body;

  try {
    const person = new Person({ name });

    await person.save();

    res.status(201).send(person);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.get('/people', async (req, res) => {
  try {
    const data = await Person.find();
    res.send(data);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.get('/', (req, res) => {
  res.send('OK');
});

(async () => {
  try {
    await mongoose.connect(`mongodb://${process.env.DB_NAME}:27017/`);
    app.listen(process.env.BACKEND_PORT, () => {
      console.log(
        `Server is running on http://localhost:${process.env.BACKEND_PORT}`
      );
    });
  } catch (err) {
    console.log(err);
  }
})();
